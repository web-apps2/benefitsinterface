<#

.SYNOPSIS
	This job queries phc_bensummary to retrieve a better error description for real-time issues.

.DESCRIPTION
	Needs a member number for input from a separate interface or parameter input.

.POWERSHELL_VERSION 
	V2
	V5

.TABLES
	phc_bensummary

.INPUT_FILES
	N/A

.OUTPUT_FILES
	No output files created, data passed back to appropriate users via email and sent back to interface.

.RERUN_INSTRUCTIONS
	Can be re-run freely.

.REVISION
	Name: Scott VanderHorst
	Date: 12/19/2019
	Description: Original Code

.KNOWN_ISSUES
	

.#>
param(
    [string] $jobNumber = "",
    [string] $AmisysConnectionStringName,
    [string] $PowershellExtract,
    [string] $MemberNumber
);
$includePath = "$(Split-Path -Path $MyInvocation.MyCommand.Path -Parent | Split-Path -Parent)\include"
. $includePath\utils.ps1;

if ($null -eq $AmisysConnectionStringName -or $AmisysConnectionStringName -eq "") {
    $AmisysConnectionStringName = "AmisysPRODOracle"
};

if ($null -eq $PowershellExtract -or $PowershellExtract -eq "") {
    $PowershellExtract = "PowershellExtract"
};

$today = get-date -f yyyyMMdd

Run-Job -JobName 'RTErrInfo' {
    if ($null -eq $MemberNumber -or $MemberNumber -eq "") {
        $MemberNumber = $MemberNum;
    };

    Write-Host "Running RealTimeErrorInfo for $MemberNumber"
    $execBenSummary = @"
exec phc_bensummary('$MemberNumber','$today');
"@
    $executed = INVOKE-SQLPLUS -MESSAGEONFAIL 'Could not query bensummary' -query $execBenSummary
    Write-Host "Error message returned for $MemberNumber is:`n"
    Write-Host "$executed"

    If ($executed -like "*bp/pg/layout*") {
        #benefit team needs to take care of this one - benefits not configured
        $body = "A real-time transaction had an error for $memberNumber. Here is the message`n$executed`n`nPlease troubleshoot this issue and notify Cathy when this may be resolved."
        Send-Email `
        -Verbose `
        -AddressDelimiter ';' `
        -EmailFrom "RealTimeErrorAnalysis@Promedica.org" `
        -EmailTo "#PHC-Benefit@ProMedica.org;Kevin.Gottfried@ProMedica.org" `
        -EmailCC "#PHC-EDI@Promedica.org;Cathy.Werner@ProMedica.org" `
        -EmailSubject "Real-time Transaction AAA42 Error Analysis: Benefits Not Configured" `
        -EmailMessage $body
        Write-Host "Email sent to Benefit, EDI, and Kevin Gottfried. Benefits Not Configured."
    }
    elseif ($executed -like "*No riders for member!*") {  
        #membership needs to take care of this one - riders not configured for group/group not renewed/rates not loaded
        $body = "A real-time transaction had an error for $memberNumber. Here is the message`n$executed`n`nPlease troubleshoot this issue and notify Cathy when this may be resolved."
        Send-Email `
        -Verbose `
        -AddressDelimiter ';' `
        -EmailFrom "RealTimeErrorAnalysis@Promedica.org" `
        -EmailTo "Kevin.Gottfried@ProMedica.org" `
        -EmailCC "#PHC-EDI@Promedica.org;Cathy.Werner@ProMedica.org" `
        -EmailSubject "Real-time Transaction AAA42 Error Analysis: Riders Not Configured, Group Not Renewed, Rates Not Loaded" `
        -EmailMessage $body
        Write-Host "Email sent to EDI and Kevin Gottfried. Riders Not Configured, Group Not Renewed, Rates Not Loaded."
    }
    elseif ($executed -like "*exception when getting layout id*") {
        #benefit team needs to take care of this one - EOB Layout
        $body = "A real-time transaction had an error for $memberNumber. Here is the message`n$executed`n`nPlease troubleshoot this issue and notify Cathy when this may be resolved."
        Send-Email `
        -Verbose `
        -AddressDelimiter ';' `
        -EmailFrom "RealTimeErrorAnalysis@Promedica.org" `
        -EmailTo "#PHC-Benefit@ProMedica.org;Kevin.Gottfried@ProMedica.org" `
        -EmailCC "#PHC-EDI@Promedica.org;Cathy.Werner@ProMedica.org" `
        -EmailSubject "Real-time Transaction AAA42 Error Analysis: EOB Layout Issue" `
        -EmailMessage $body
        Write-Host "Email sent to Benefit, EDI, and Kevin Gottfried. EOB Layout Issue."
    }
    elseif ($executed -like "*exception getting BENEFIT_PKG*") {
        #membership needs to take care of this - member may be termed or bad member num
        $body = "A real-time transaction had an error for $memberNumber. Here is the message`n$executed`n`nPlease troubleshoot this issue and notify Cathy when this may be resolved."
        Send-Email `
        -Verbose `
        -AddressDelimiter ';' `
        -EmailFrom "RealTimeErrorAnalysis@Promedica.org" `
        -EmailTo "Michelle.White@ProMedica.org;Kevin.Gottfried@ProMedica.org" `
        -EmailCC "#PHC-EDI@Promedica.org;Cathy.Werner@ProMedica.org" `
        -EmailSubject "Real-time Transaction AAA42 Error Analysis: Member May Be Termed or Bad Member Number" `
        -EmailMessage $body
        Write-Host "Email sent to Michelle White, EDI, and Kevin Gottfried. Member May Be Termed or it was a bad member number."
    }
    elseif ($executed -like "*ct_time_spans*") {
        #membership or Matt needs to take care of this - contiguous spans are bad
        $body = "A real-time transaction had an error for $memberNumber. Here is the message`n$executed`n`nPlease troubleshoot this issue and notify Cathy when this may be resolved."
        Send-Email `
        -Verbose `
        -AddressDelimiter ';' `
        -EmailFrom "RealTimeErrorAnalysis@Promedica.org" `
        -EmailTo "Matt.Elder@ProMedica.org;Kevin.Gottfried@ProMedica.org" `
        -EmailCC "#PHC-EDI@Promedica.org;Cathy.Werner@ProMedica.org" `
        -EmailSubject "Real-time Transaction AAA42 Error Analysis: Contiguous Spans are Bad" `
        -EmailMessage $body
        Write-Host "Email sent to Matt Elder, EDI, and Kevin Gottfried. Contiguous Spans are Bad."
    }
    elseif ($executed -like "*The as of date provided,*") {
        #membership or Matt needs to take care of this - date might be funky?
        $body = "A real-time transaction had an error for $memberNumber. Here is the message`n$executed`n`nPlease troubleshoot this issue and notify Cathy when this may be resolved."
        Send-Email `
        -Verbose `
        -AddressDelimiter ';' `
        -EmailFrom "RealTimeErrorAnalysis@Promedica.org" `
        -EmailTo "Matt.Elder@ProMedica.org;Kevin.Gottfried@ProMedica.org" `
        -EmailCC "#PHC-EDI@Promedica.org;Cathy.Werner@ProMedica.org" `
        -EmailSubject "Real-time Transaction AAA42 Error Analysis: Date Issue" `
        -EmailMessage $body
        Write-Host "Email sent to Matt Elder, EDI, and Kevin Gottfried. Date Issue."
    }
    elseif ($executed -like "*procedure successful*") {
        #no errors
        $body = "A real-time transaction had an error for $memberNumber. The stored procedure however returned`n$executed`n`nThis issue appears to be resolved."
        Send-Email `
        -Verbose `
        -AddressDelimiter ';' `
        -EmailFrom "RealTimeErrorAnalysis@Promedica.org" `
        -EmailTo "Cathy.Werner@ProMedica.org" `
        -EmailCC "#PHC-EDI@Promedica.org" `
        -EmailSubject "Real-time Transaction AAA42 Error Analysis: No Error on $memberNum." `
        -EmailMessage $body
        Write-Host "Email sent to EDI. No Error on $memberNum"
    }
    else {
        #something strange happened
        $body = "A real-time transaction had an error for $memberNumber. The stored procedure however returned`n$executed`n`nThis issue is odd. We'll need to look in to this one."
        Send-Email `
        -Verbose `
        -AddressDelimiter ';' `
        -EmailFrom "RealTimeErrorAnalysis@Promedica.org" `
        -EmailTo "Matt.Elder@ProMedica.org;Kevin.Gottfried@ProMedica.org" `
        -EmailCC "#PHC-EDI@Promedica.org;Cathy.Werner@ProMedica.org" `
        -EmailSubject "Real-time Transaction AAA42 Error Analysis: Undefined Error" `
        -EmailMessage $body
        Write-Host "Email sent to Matt Elder, EDI, and Kevin Gottfried. Undefined Error."
    }
    $SqlString=@"
UPDATE configInputParms
SET ParmValue = '0', LastUpdateUser = 'RTErrInfoPS'
WHERE JobNumber = 'RTErrInfo' AND ParmName = 'Running'
"@;
    $null = Invoke-SqlCommand -connectionStringName "PowershellExtract" -SqlCommand $sqlString
    Write-Host "Process completed successfully." -ForegroundColor Green
}