using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using BenSummaryInterface;
using BenSummaryInterface.Controllers;

namespace BenSummaryInterface.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTest
    {
        [Test]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
        [Test]
        public void Return()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Return() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
