using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Newtonsoft.Json;

namespace BenSummaryInterface.Controllers
{
    [Authorize(Roles = "Paramount Administrators,PHC_EDI_Development,#Corp - Epic Core Revenue Cycle")]
    public class HomeController : Controller
    {
        public string JobStatus { get; set; }
        public ActionResult Index()
        {
            ViewBag.Title = "BenSummary Interface";

            return View();
        }
        public ActionResult Return()
        {
            ViewBag.Title = "BenSummary Interface";

            return View();
        }
        [HttpPost]
        public ActionResult UpdateMemberNbr(string MemberNum)
        {

            string flagPath;
            string flagName;
            DateTime now = DateTime.Now;

            using (DataAccess PSE = new DataAccess(ConfigurationManager.ConnectionStrings["PowershellExtractNoProv"].ConnectionString))
            {
                PSE.OpenConn();
                PSE.ExecuteScalar<string>($"UPDATE configInputParms SET ParmValue = '{MemberNum}' WHERE JobNumber = 'RTErrInfo' AND ParmName = 'MemberNum';");
                PSE.ExecuteScalar<DateTime>($"UPDATE configInputParms SET LastUpdateDate = '{now}' WHERE JobNumber = 'RTErrInfo' AND ParmName = 'MemberNum';");
                PSE.ExecuteScalar<string>("UPDATE configInputParms SET LastUpdateUser = 'RtErrInfoApp' WHERE JobNumber = 'RTErrInfo' AND ParmName = 'MemberNum';");
                PSE.ExecuteScalar<string>("UPDATE configInputParms SET ParmValue = '1' WHERE JobNumber = 'RTErrInfo' AND ParmName = 'Running';");
                PSE.ExecuteScalar<DateTime>($"UPDATE configInputParms SET LastUpdateDate = '{now}' WHERE JobNumber = 'RTErrInfo' AND ParmName = 'Running';");
                PSE.ExecuteScalar<string>("UPDATE configInputParms SET LastUpdateUser = 'RtErrInfoApp' WHERE JobNumber = 'RTErrInfo' AND ParmName = 'Running';");
            }
            using (DataAccess PSE = new DataAccess(ConfigurationManager.ConnectionStrings["PowershellExtractNoProv"].ConnectionString))
            {
                PSE.OpenConn();
                flagPath = PSE.ExecuteScalar<string>("SELECT ParmValue FROM configInputParms WHERE JobNumber = 'RTErrInfo' AND ParmName = 'FlagPath';");
                flagName = PSE.ExecuteScalar<string>("SELECT ParmValue FROM configInputParms WHERE JobNumber = 'RTErrInfo' AND ParmName = 'FlagName';");

            }
            StreamWriter writer = new StreamWriter(
                Path.Combine(
                    flagPath,
                    flagName)
                );
            writer.Close();
            writer.Close();
            return Json("true");
        }
        [HttpGet]
        public ActionResult StatusCheck(string JobStatus)
        {
           // string JobStatus;
            using (DataAccess PSE = new DataAccess(ConfigurationManager.ConnectionStrings["PowershellExtractNoProv"].ConnectionString))
            {
                PSE.OpenConn();
                JobStatus = PSE.ExecuteScalar<string>("SELECT ParmValue FROM configInputParms WHERE JobNumber = 'RTErrInfo' AND ParmName = 'Running';");
            }
            return Json(JobStatus,JsonRequestBehavior.AllowGet);
        }
    }
}
