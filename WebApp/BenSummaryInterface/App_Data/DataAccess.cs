﻿using Dapper;
using DapperExtensions;
using System;
using System.Collections.Generic;
//using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Configuration;

namespace BenSummaryInterface
{
    [Obsolete("Consider adding this functionality to PHCDataLayer instead of a standalone class")]
    public class DataAccess : IDisposable
    {
        /// <summary>
        /// Private connection string value. Can't be reset after instantiation
        /// </summary>        
        private string DbConnStr { get; set; }
        /// <summary>
        /// The connection
        /// </summary>
        private IDbConnection Connection { get; set; }
        /// <summary>
        /// Flag denotion of object being disposed
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Instantiates connection string
        /// </summary>
        /// <param name="connStr">Full connection string to Database</param>
        public DataAccess(string connStr)
        {
            DbConnStr = connStr;
            IsOpen = false;
        }

        /// <summary>
        /// Open a connection to the database specified in DbConnStr
        /// </summary>
        public void OpenConn(string connType = "mssql")
        {
            if (connType == "mssql")
            {
                Connection = new SqlConnection(DbConnStr);
                Connection.Open();
                IsOpen = true;
            }
            //else if (connType == "oracle")
            //{
            //    Connection = new OracleConnection(DbConnStr);
            //    Connection.Open();
            //    IsOpen = true;
            //}
            else
            {
                throw new InvalidOperationException("Unrecognized connection type");
            }
        }

        /// <summary>
        /// Tells whether the connection is open
        /// </summary>
        public bool IsOpen { get; set; }

        /// <summary>
        /// Return an Ienumerable from a Select query
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">Must be a select query</param>
        /// <returns></returns>
        public IEnumerable<T> GetEnumeration<T>(string sql)
        {
            if (!IsOpen)
            {
                throw new Exception("Connection is not open");
            }
            else
            {
                return Connection.Query<T>(sql).AsEnumerable();
            }
        }

        /// <summary>
        /// Insert an object with a custom insert statement
        /// </summary>
        /// <param name="sql"></param>
        public void InsertObject(string sql)
        {
            if (!IsOpen)
            {
                throw new Exception("Connection is not open");
            }
            else
            {
                _ = Connection.Execute(sql);
            }
        }

        /// <summary>
        /// Insert an object into a table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public int InsertObject<T>(T obj) where T : class
        {
            if (!IsOpen)
            {
                throw new Exception("Connection is not open");
            }
            else
            {
                return Connection.Insert(obj);
            }
        }

        /// <summary>
        /// Insert an enumerable of type T into a table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public void InsertObjectList<T>(IEnumerable<T> obj) where T : class
        {
            if (!IsOpen)
            {
                throw new Exception("Connection is not open");
            }
            else
            {
                Connection.Insert(obj);
            }
        }

        /// <summary>
        /// Execute parameterized SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public int Execute(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            if (!IsOpen)
            {
                throw new Exception("Connection is not open");
            }
            else
            {
                return Connection.Execute(sql, param, transaction, commandTimeout, commandType);
            }
        }

        /// <summary>
        /// Execute paramaterized SQL
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public T ExecuteScalar<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            if (!IsOpen)
            {
                throw new Exception("Connection is not open");
            }
            else
            {
                return Connection.ExecuteScalar<T>(sql, param, transaction, commandTimeout, commandType);
            }
        }

        /// <summary>
        /// Execute paramaterized SQL
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public IEnumerable<T> Query<T>(string sql, object param = null, IDbTransaction transaction = null, bool buffered=true, int? commandTimeout = null, CommandType? commandType = null)
        {
            return Connection.Query<T>(sql, param, transaction, buffered, commandTimeout, commandType);
        }

        /// <summary>
        /// Updates a single object row in the database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="ignoreAllKeyProperties"></param>
        /// <returns></returns>
        [Obsolete("This doesn't work", true)]
        public bool Update<T>(T obj, IDbTransaction transaction = null, int? commandTimeout = null, bool ignoreAllKeyProperties = false)
            where T : class
        {
            return Connection.Update(obj, transaction, commandTimeout, ignoreAllKeyProperties);
        }

        /// <summary>
        /// Updates a list of objects in the database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="ignoreAllKeyProperties"></param>
        /// <returns></returns>
        [Obsolete("This doesn't work", true)]
        public bool Update<T>(IEnumerable<T> obj, IDbTransaction transaction = null, int? commandTimeout = null, bool ignoreAllKeyProperties = false)
            where T : class
        {
            return Connection.Update(obj, transaction, commandTimeout, ignoreAllKeyProperties);
        }

        /// <summary>
        /// Close and dispose of connection
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of object
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                Connection.Close();
                Connection.Dispose();
                IsOpen = false;
            }

            disposed = true;
        }
    }
}
