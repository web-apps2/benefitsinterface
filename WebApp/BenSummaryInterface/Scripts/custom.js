function mbrNumLength() {
    var x = document.getElementById("MemberNbr").value;
    var y = document.getElementById("divError");
    var z = document.getElementById("btnSubmit");
    if (x.length != 11) {
                z.style.visibility = "hidden";
                y.style.visibility = "visible";
    } else {
                z.style.visibility = "visible";
                y.style.visibility = "hidden";
    }
}

function btnSubmit() {
    var memberNum = document.getElementById("MemberNbr").value;
    var x = document.getElementById("btnSubmit");
    x.disabled = true;
    x.style.color = "LightGray";
    $.ajax({
        type: "POST",
        url: "/bensummaryinterface/home/updatemembernbr",
        data: { "MemberNum": memberNum },
        success: function (data) {
            $(location).attr('href', "/bensummaryinterface/Home/Return");
        }
    });
}

function runningStatus() {
    var jobStatus;
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/bensummaryinterface/home/statuscheck",
        data: { jobStatus: "JobStatus" },
        success: statusInfo
    });
}

function statusInfo(response) {
    response = response;
    if (response == "0") {
        document.getElementById("btnSubmit").value = "Submit";
        document.getElementById("btnSubmit").disabled = false;
        document.getElementById("btnSubmit").style.color = "Gray";
    } else {
        document.getElementById("btnSubmit").value = "Script hasn't finished running. Page will auto-refresh until script complete";
        document.getElementById("btnSubmit").disabled = true;
        document.getElementById("btnSubmit").style.color = "LightGray";
        window.setTimeout(reloadPage, 10000);
    }
}

function reloadPage() {
    window.location.reload();
}
