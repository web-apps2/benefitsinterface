using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BenSummaryInterface
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "UpdateMemberNbr",
                url: "bensummaryinterface/{controller}/{action}",
                defaults: new { controller = "Home", action = "UpdateMemberNbr" }
            );
            routes.MapRoute(
                name: "Return",
                url: "bensummaryinterface/{controller}/{action}",
                defaults: new { controller = "Home", action = "Return" }
            );
            routes.MapRoute(
                name: "StatusCheck",
                url: "bensummaryinterface/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "StatusCheck", id = "JobStatus" }
            );
        }
    }
}
